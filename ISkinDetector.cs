using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV.Structure;
using Emgu.CV;



public interface ISkinDetector
{
    /// <summary>
    /// Detects skin in the given image and returns a binary mask where skin pixels are white.
    /// </summary>
    /// <param name="image">The source image in BGR color space.</param>
    /// <returns>A binary image mask indicating skin regions.</returns>
    Image<Gray, Byte> DetectSkin(Image<Bgr, Byte> image);
}
