using Emgu.CV; // Import Emgu.CV namespace for VideoCapture and Mat classes.

// Class implementing the IFrameCapture interface for capturing frames from a camera.
public class CameraCapture : IFrameCapture
{
    private VideoCapture _capture; // VideoCapture object to capture frames from the camera.

    // Constructor to initialize the CameraCapture object.
    public CameraCapture()
    {
        _capture = new VideoCapture(0); // Initialize the VideoCapture object with device index 0 (default camera).
    }

    // Method to capture a single frame.
    // Returns:
    //   - A Mat object representing the captured frame.
    public Mat CaptureFrame()
    {
        return _capture.QueryFrame(); // Return the next frame from the video capture device.
    }

    // Method to start the frame capture process.
    public void Start()
    {
        _capture.Start(); // Start capturing frames from the camera.
    }

    // Method to stop the frame capture process.
    public void Stop()
    {
        _capture.Stop(); // Stop capturing frames from the camera.
    }
}

