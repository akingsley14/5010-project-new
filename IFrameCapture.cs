using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV.Structure;
using Emgu.CV;


// Interface for capturing frames from a source.
public interface IFrameCapture
{
    // Method signature for capturing a single frame.
    // Returns:
    //   - A Mat object representing the captured frame.
    Mat CaptureFrame();

    // Method signature for starting the frame capture process.
    void Start();

    // Method signature for stopping the frame capture process.
    void Stop();
}
