using Emgu.CV.Util;
using Emgu.CV; // Import Emgu.CV namespace for Mat, Image, and VectorOfPoint classes.
using Emgu.CV.CvEnum; // Import Emgu.CV.CvEnum namespace for RetrType and ChainApproxMethod enums.
using Emgu.CV.Structure; // Import Emgu.CV.Structure namespace for Bgr and Gray classes.
using System; // Import System namespace for basic types and utilities.
using System.Drawing; // Import System.Drawing namespace for Color and Point classes.
using System.Runtime.InteropServices; // Import System.Runtime.InteropServices namespace for Marshal class.

// Class implementing the IGestureRecognition interface for basic gesture recognition.
public class BasicGestureRecognition : IGestureRecognition
{
    // Method to draw contour, convex hull, and finger count on the frame.
    private Image<Bgr, byte> DrawContourAndHull(Image<Bgr, byte> frame, VectorOfPoint contour, VectorOfInt hullIndices, int fingerCount)
    {
        // Draw the detected contour.
        frame.DrawPolyline(contour.ToArray(), true, new Bgr(Color.LimeGreen), 2);

        // Convert hull indices to points.
        Point[] hullPoints = hullIndices.ToArray().Select(index => contour[index]).ToArray();

        // Draw the convex hull.
        frame.DrawPolyline(hullPoints, true, new Bgr(Color.DarkRed), 2);

        // Draw the finger count on the image.
        string text = $"Fingers: {fingerCount}";
        CvInvoke.PutText(frame, text, new Point(10, 50), FontFace.HersheyComplex, 1.0, new MCvScalar(23, 163, 232), 2);

        return frame;
    }

    // Method to recognize gestures and draw them on the frame.
    public (string, Image<Bgr, byte>) RecognizeGestureAndDraw(Image<Bgr, byte> frame, Image<Gray, byte> skinImage)
    {
        // Find contours in the skin image.
        var contours = new VectorOfVectorOfPoint();
        CvInvoke.FindContours(skinImage, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple);

        // Find the biggest contour among all contours.
        VectorOfPoint biggestContour = FindBiggestContour(contours);

        if (biggestContour != null && biggestContour.Size > 0)
        {
            // Extract the convex hull and defects.
            var hull = new VectorOfInt();
            CvInvoke.ConvexHull(biggestContour, hull, false);
            var defects = new Mat();
            CvInvoke.ConvexityDefects(biggestContour, hull, defects);

            // Process the defects and count fingers.
            int fingerCount = ProcessDefects(defects, biggestContour, frame);

            // Adjust finger count (optional).
            if (fingerCount >= 0)
            {
                fingerCount += 1;
            }
            else
            {
                fingerCount += 0;
            }

            // Draw the contour and the convex hull on the frame.
            var imageWithDrawing = DrawContourAndHull(frame, biggestContour, hull, fingerCount);

            // Return the recognized gesture (finger count) and the image with drawing.
            return (fingerCount.ToString(), imageWithDrawing);
        }

        // Return "0" and original frame if no gestures are recognized.
        return ("0", frame);
    }

    // Method to find the biggest contour among all contours.
    private VectorOfPoint FindBiggestContour(VectorOfVectorOfPoint contours)
    {
        double maxArea = 0;
        VectorOfPoint biggestContour = null;

        // Iterate through all contours to find the one with the maximum area.
        for (int i = 0; i < contours.Size; i++)
        {
            // Calculate the area of the current contour.
            double area = CvInvoke.ContourArea(contours[i], false);
            if (area > maxArea)
            {
                // Update the maximum area and the corresponding contour.
                maxArea = area;
                biggestContour = new VectorOfPoint(contours[i].ToArray());
            }
        }
        return biggestContour; // Return the biggest contour found.
    }

    // Method to process defects and count fingers.
    private int ProcessDefects(Mat defects, VectorOfPoint contour, Image<Bgr, byte> frame)
    {
        if (defects.IsEmpty || defects.Rows == 0) return 0;

        int fingerCount = 0;
        for (int i = 0; i < defects.Rows; i++)
        {
            // Extract defect information.
            int[] defectData = new int[4];
            Marshal.Copy(defects.DataPointer + i * defects.Step, defectData, 0, 4);
            Point startPoint = contour[defectData[0]];
            Point endPoint = contour[defectData[1]];
            Point farPoint = contour[defectData[2]];
            double depth = defectData[3] / 256.0;

            // Calculate the angle at the "far" point.
            double angle = CalculateAngle(startPoint, farPoint, endPoint);

            // Example thresholds, these may need to be adjusted.
            double depthThreshold = 40.0; // Adjust as necessary.
            double angleThreshold = 90.0; // Adjust as necessary, considering angles less than 90 degrees.

            // Check if the defect represents a finger based on depth and angle.
            if (depth > depthThreshold && angle < angleThreshold)
            {
                fingerCount++;
                // draw the defect for visualization.
                CvInvoke.Circle(frame, farPoint, 5, new MCvScalar(200, 223, 142), 2);
            }
        }

        return fingerCount; // Return the total finger count.
    }

    // Method to calculate the angle between three points.
    private double CalculateAngle(Point start, Point far, Point end)
    {
        // Calculate the length of the sides of the triangle formed by the three points.
        double a = Math.Sqrt(Math.Pow(end.X - start.X, 2) + Math.Pow(end.Y - start.Y, 2));
        double b = Math.Sqrt(Math.Pow(far.X - start.X, 2) + Math.Pow(far.Y - start.Y, 2));
        double c = Math.Sqrt(Math.Pow(end.X - far.X, 2) + Math.Pow(end.Y - far.Y, 2));

        // Use the cosine rule to calculate the angle at the "far" point.
        double angle = Math.Acos((b * b + c * c - a * a) / (2 * b * c));

        // Convert the angle from radians to degrees.
        angle = angle * (180 / Math.PI);
        return angle; // Return the calculated angle.
    }
}

