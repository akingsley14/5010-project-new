VisionBot

Hand Signal Controlled Robot using EmguCV

Overview

This C# project allows a user to control a robot using hand signals captured from their
camera. It utilizes EmguCV, a .NET wrapper for OpenCV, for real-time computer vision processing 
to recognize hand gestures and translate them into commands for controlling the robot.

Features

- Real-time hand gesture recognition using EmguCV.
- Mapping hand gestures to robot control commands.
- Integration with a ZUMO 32U4 robot control system.
- User-friendly interface for observing and controlling the robot.
- Ability to speed up and slow down pace of robot

Requirements

- .NET Framework or .NET Core.
- EmguCV library (installable via NuGet).
- Robot control hardware (e.g., Arduino or Raspberry Pi) and Zumo32U4 robot kit 
  and USB - microUSB cable for communication.
- Camera or webcam for capturing hand signals.

Installation

1. Clone or download the project repository from GitHub.
2. Ensure that EmguCV is properly installed and referenced in the project.
3. Set up the hardware for controlling the robot and establish communication with the C# application.
4. Connect the camera or webcam to the system.
5. Build the solution in your preferred IDE or using the command line.

Usage

1. Run the compiled executable or start the application from your IDE.
2. Ensure that the camera is properly configured (plain background) and capturing the hand signals.
3. If necessary, adjust for different lighting conditions or hand positions.
4. Perform hand gestures according to predefined commands to control the robot.
   (default to 1 finger for stop, 2 for forwards, 3 for backwards, 4 for turn left, and 5 for turn right)
5. Use the bottons in the UI to start and control robot speed. (press start camera to initialize camera, 
   start control to start sending controls, and the speed up and speed down buttons to control 
   speed of the robot )
6. Adjust settings or add new gestures as needed to improve performance or functionality.

