using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV.Structure;
using Emgu.CV;

// This is the entry point of the application.
static class Program
{
    // Main method, starting point of the application.
    [STAThread] // Indicates that the COM threading model for the application is single-threaded apartment (STA).
    static void Main()
    {
        // Enable visual styles for the application.
        Application.EnableVisualStyles();
        
        // Sets the application to use the default text rendering.
        Application.SetCompatibleTextRenderingDefault(false);

        // Initialize frame capture component. 
        IFrameCapture frameCapture = new CameraCapture(); // Using CameraCapture class to capture frames.
        
        // Initialize skin detection component.
        ISkinDetector skinDetector = new YCrCbSkinDetector(); // Using YCrCbSkinDetector for skin detection.
        
        // Initialize gesture recognition component.
        IGestureRecognition gestureRecognition = new BasicGestureRecognition(); // Using BasicGestureRecognition for gesture recognition.

        // Run the main form with the initialized components.
        Application.Run(new MainForm(frameCapture, skinDetector, gestureRecognition)); // MainForm is initiated with frameCapture, skinDetector, and gestureRecognition.
    }
}

