using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV.Structure;
using Emgu.CV;


// Interface for gesture recognition functionality.
public interface IGestureRecognition
{
    // Method signature for recognizing gestures and drawing them on a frame.
    // Parameters:
    //   - frame: The original image frame in Bgr format.
    //   - skinImage: The binary mask image representing detected skin regions in Gray format.
    // Returns:
    //   - A tuple containing:
    //     1. A string representing the recognized gesture.
    //     2. An Image<Bgr, byte> object representing the frame with gestures drawn on it.
    (string, Image<Bgr, byte>) RecognizeGestureAndDraw(Image<Bgr, byte> frame, Image<Gray, byte> skinImage);
}
