using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV.Structure;
using Emgu.CV;
using System.IO.Ports;


// Partial class for the main form of the application.
public partial class MainForm : Form
{
    // Fields to hold instances of components/interfaces.
    private readonly IFrameCapture _frameCapture;
    private readonly ISkinDetector _skinDetector;
    private readonly IGestureRecognition _gestureRecognition;

    private bool Command = false;


    // Constructor to initialize MainForm with required components.
    public MainForm(IFrameCapture frameCapture, ISkinDetector skinDetector, IGestureRecognition gestureRecognition)
    {
        InitializeComponent();

        // Initialize components/interfaces.
        _frameCapture = frameCapture;
        _skinDetector = skinDetector;
        _gestureRecognition = gestureRecognition;



        SerialPort serialPort;
        serialPort = new SerialPort("COM3", 9600);

    }

    // Method to process each captured frame.
    void FrameGrabber(object sender, EventArgs e)
    {
        // Capture a frame from the frame capture component.
        using (var frame = _frameCapture.CaptureFrame())
        {
            if (frame != null)
            {
                // Convert the captured frame to an EmguCV image.
                var image = frame.ToImage<Bgr, Byte>();

                // Detect skin in the captured frame.
                var skin = _skinDetector.DetectSkin(image);

                // Recognize gesture and draw on the image.
                var (gesture, imageWithDrawing) = _gestureRecognition.RecognizeGestureAndDraw(image, skin);

                // Display the frame with detected gesture.
                imageBoxFrameGrabber.Image = imageWithDrawing;

                // Update UI elements on the main thread.
                this.Invoke((MethodInvoker)delegate
                {
                    // Update UI elements with relevant data.
                    imageBoxSkin.Image = skin; // Display the binary mask image after skin detection.
                    imageBoxFrameGrabber.Image = image; // Display the original frame possibly with annotations.
                    Console.WriteLine($"Detected Gesture: {gesture}"); // Output detected gesture to the console.
                });

                if (Command == true)
                {
                    RobotControl.Control(gesture);

                }


            }
        }
    }

    // Method to update UI elements safely across threads.
    private void UpdateUI(Action updateAction)
    {
       
        if (this.InvokeRequired)
        {
            this.Invoke(new MethodInvoker(updateAction));
        }
        else
        {
            updateAction();
        }
    }

    // Event handler for the start button click.
    private void startButton_Click(object sender, EventArgs e)
    {
        // Check if frame capture component exists.
        if (_frameCapture != null)
        {
            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

            timer.Interval = 33; // Update approximately every 30 frames per second
            timer.Tick += new EventHandler(FrameGrabber);
            timer.Start();

            _frameCapture.Start(); // Start capturing frames.

        }
    }

    private void imageBoxFrameGrabber_Click(object sender, EventArgs e)
    {

    }

    private void MainForm_Load(object sender, EventArgs e)
    {

    }

    private void button1_Click(object sender, EventArgs e)
    {
        Command=true;
    }

    private void imageBoxSkin_Click(object sender, EventArgs e)
    {

    }


    private void IncreaseSpeedButton_Click_1(object sender, EventArgs e)
    {
        RobotControl.Control("+");
    }

    private void DecreaseSpeedButton_Click_1(object sender, EventArgs e)
    {
        RobotControl.Control("-");
    }
}
