﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV.Structure;
using Emgu.CV;
using System.IO.Ports;


public class RobotControl
{
    private static RobotControl robotControl;
    private static SerialPort serialPort = new SerialPort("COM3", 9600);
    private RobotControl() { }

    public static void Control(string gesture)
    {


        try
        {
            if (!serialPort.IsOpen)
            {
                serialPort.Open();
                Console.WriteLine("Port opened successfully");
            }

        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }


        if (gesture != null)
        {
            if (gesture == "1")
            {
                serialPort.Write("1");
            }
            if (gesture == "2")
            {
                serialPort.Write("2");
            }
            if (gesture == "3")
            {
                serialPort.Write("3");
            }
            if (gesture == "4")
            {
                serialPort.Write("4");
            }
            if (gesture == "5")
            {
                serialPort.Write("5");
            }
            if (gesture == "+")
            {
                serialPort.Write("+");
            }
            if (gesture == "-")
            {
                serialPort.Write("-");
            }
        }
    }


}
