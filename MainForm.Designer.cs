using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV.Structure;
using Emgu.CV;


partial class MainForm
{
    /// Required designer variable.
    private System.ComponentModel.IContainer components = null;

    /// Clean up any resources being used.
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code


    private void InitializeComponent()
    {
        components = new Container();
        ComponentResourceManager resources = new ComponentResourceManager(typeof(MainForm));
        startButton = new Button();
        imageBoxFrameGrabber = new Emgu.CV.UI.ImageBox();
        imageBoxSkin = new Emgu.CV.UI.ImageBox();
        panel1 = new Panel();
        DecreaseSpeedButton = new Button();
        IncreaseSpeedButton = new Button();
        button1 = new Button();
        pictureBox1 = new PictureBox();
        imageBox1 = new Emgu.CV.UI.ImageBox();
        ((ISupportInitialize)imageBoxFrameGrabber).BeginInit();
        ((ISupportInitialize)imageBoxSkin).BeginInit();
        panel1.SuspendLayout();
        ((ISupportInitialize)pictureBox1).BeginInit();
        ((ISupportInitialize)imageBox1).BeginInit();
        SuspendLayout();
        // 
        // startButton
        // 
        startButton.FlatAppearance.BorderSize = 0;
        startButton.FlatStyle = FlatStyle.Flat;
        startButton.Font = new Font("Nirmala UI", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 0);
        startButton.ForeColor = Color.White;
        startButton.ImageAlign = ContentAlignment.MiddleRight;
        startButton.Location = new Point(4, 222);
        startButton.Margin = new Padding(4, 5, 4, 5);
        startButton.Name = "startButton";
        startButton.Size = new Size(149, 87);
        startButton.TabIndex = 0;
        startButton.Text = "Start Camera";
        startButton.UseVisualStyleBackColor = true;
        startButton.Click += startButton_Click;
        // 
        // imageBoxFrameGrabber
        // 
        imageBoxFrameGrabber.BackColor = Color.White;
        imageBoxFrameGrabber.BackgroundImageLayout = ImageLayout.Stretch;
        imageBoxFrameGrabber.InitialImage = (Image)resources.GetObject("imageBoxFrameGrabber.InitialImage");
        imageBoxFrameGrabber.Location = new Point(164, 28);
        imageBoxFrameGrabber.Margin = new Padding(4, 5, 4, 5);
        imageBoxFrameGrabber.Name = "imageBoxFrameGrabber";
        imageBoxFrameGrabber.Size = new Size(677, 642);
        imageBoxFrameGrabber.SizeMode = PictureBoxSizeMode.Zoom;
        imageBoxFrameGrabber.TabIndex = 2;
        imageBoxFrameGrabber.TabStop = false;
        imageBoxFrameGrabber.Click += imageBoxFrameGrabber_Click;
        // 
        // imageBoxSkin
        // 
        imageBoxSkin.BackColor = Color.White;
        imageBoxSkin.BackgroundImageLayout = ImageLayout.Zoom;
        imageBoxSkin.Location = new Point(863, 28);
        imageBoxSkin.Margin = new Padding(4, 5, 4, 5);
        imageBoxSkin.Name = "imageBoxSkin";
        imageBoxSkin.Size = new Size(677, 642);
        imageBoxSkin.SizeMode = PictureBoxSizeMode.Zoom;
        imageBoxSkin.TabIndex = 4;
        imageBoxSkin.TabStop = false;
        imageBoxSkin.Click += imageBoxSkin_Click;
        // 
        // panel1
        // 
        panel1.BackColor = Color.FromArgb(24, 30, 54);
        panel1.Controls.Add(DecreaseSpeedButton);
        panel1.Controls.Add(IncreaseSpeedButton);
        panel1.Controls.Add(button1);
        panel1.Controls.Add(startButton);
        panel1.Controls.Add(pictureBox1);
        panel1.Dock = DockStyle.Left;
        panel1.Location = new Point(0, 0);
        panel1.Name = "panel1";
        panel1.Size = new Size(157, 710);
        panel1.TabIndex = 5;
        // 
        // DecreaseSpeedButton
        // 
        DecreaseSpeedButton.FlatAppearance.BorderSize = 0;
        DecreaseSpeedButton.FlatStyle = FlatStyle.Flat;
        DecreaseSpeedButton.Font = new Font("Nirmala UI", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 0);
        DecreaseSpeedButton.ForeColor = Color.White;
        DecreaseSpeedButton.ImageAlign = ContentAlignment.MiddleRight;
        DecreaseSpeedButton.Location = new Point(4, 513);
        DecreaseSpeedButton.Margin = new Padding(4, 5, 4, 5);
        DecreaseSpeedButton.Name = "DecreaseSpeedButton";
        DecreaseSpeedButton.Size = new Size(149, 87);
        DecreaseSpeedButton.TabIndex = 3;
        DecreaseSpeedButton.Text = "Speed(-)";
        DecreaseSpeedButton.UseVisualStyleBackColor = true;
        DecreaseSpeedButton.Click += DecreaseSpeedButton_Click_1;
        // 
        // IncreaseSpeedButton
        // 
        IncreaseSpeedButton.FlatAppearance.BorderSize = 0;
        IncreaseSpeedButton.FlatStyle = FlatStyle.Flat;
        IncreaseSpeedButton.Font = new Font("Nirmala UI", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 0);
        IncreaseSpeedButton.ForeColor = Color.White;
        IncreaseSpeedButton.ImageAlign = ContentAlignment.MiddleRight;
        IncreaseSpeedButton.Location = new Point(4, 416);
        IncreaseSpeedButton.Margin = new Padding(4, 5, 4, 5);
        IncreaseSpeedButton.Name = "IncreaseSpeedButton";
        IncreaseSpeedButton.Size = new Size(149, 87);
        IncreaseSpeedButton.TabIndex = 2;
        IncreaseSpeedButton.Text = "Speed(+)";
        IncreaseSpeedButton.UseVisualStyleBackColor = true;
        IncreaseSpeedButton.Click += IncreaseSpeedButton_Click_1;
        // 
        // button1
        // 
        button1.FlatAppearance.BorderSize = 0;
        button1.FlatStyle = FlatStyle.Flat;
        button1.Font = new Font("Nirmala UI", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 0);
        button1.ForeColor = Color.White;
        button1.ImageAlign = ContentAlignment.MiddleRight;
        button1.Location = new Point(4, 319);
        button1.Margin = new Padding(4, 5, 4, 5);
        button1.Name = "button1";
        button1.Size = new Size(149, 87);
        button1.TabIndex = 1;
        button1.Text = "Start Control";
        button1.UseVisualStyleBackColor = true;
        button1.Click += button1_Click;
        // 
        // pictureBox1
        // 
        //pictureBox1.BackgroundImage = _5010_project_new.Properties.Resources.Modern_Creative_Technology_Logo__2_;
        //pictureBox1.Image = _5010_project_new.Properties.Resources.Modern_Creative_Technology_Logo__2_;
        pictureBox1.Location = new Point(-60, -31);
        pictureBox1.Name = "pictureBox1";
        pictureBox1.Size = new Size(266, 245);
        pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
        pictureBox1.TabIndex = 0;
        pictureBox1.TabStop = false;
        // 
        // imageBox1
        // 
        imageBox1.BackgroundImage = (Image)resources.GetObject("imageBox1.BackgroundImage");
        imageBox1.BackgroundImageLayout = ImageLayout.Stretch;
        imageBox1.Dock = DockStyle.Fill;
        imageBox1.Location = new Point(0, 0);
        imageBox1.Name = "imageBox1";
        imageBox1.Size = new Size(1595, 710);
        imageBox1.TabIndex = 2;
        imageBox1.TabStop = false;
        // 
        // MainForm
        // 
        AutoScaleDimensions = new SizeF(8F, 20F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(46, 51, 73);
        BackgroundImageLayout = ImageLayout.Stretch;
        ClientSize = new Size(1595, 710);
        Controls.Add(panel1);
        Controls.Add(imageBoxSkin);
        Controls.Add(imageBoxFrameGrabber);
        Controls.Add(imageBox1);
        Margin = new Padding(4, 5, 4, 5);
        Name = "MainForm";
        Text = "Hand Gesture Recognition";
        Load += MainForm_Load;
        ((ISupportInitialize)imageBoxFrameGrabber).EndInit();
        ((ISupportInitialize)imageBoxSkin).EndInit();
        panel1.ResumeLayout(false);
        ((ISupportInitialize)pictureBox1).EndInit();
        ((ISupportInitialize)imageBox1).EndInit();
        ResumeLayout(false);
    }

    #endregion

    private System.Windows.Forms.Button startButton;
    private Emgu.CV.UI.ImageBox imageBoxFrameGrabber;
    private Emgu.CV.UI.ImageBox imageBoxSkin;
    private Panel panel1;
    private PictureBox pictureBox1;
    private Button button1;
    private Button DecreaseSpeedButton;
    private Button IncreaseSpeedButton;
    private Emgu.CV.UI.ImageBox imageBox1;
}
