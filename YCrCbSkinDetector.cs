using Emgu.CV.Structure; // Import Emgu.CV.Structure namespace for Image<Gray, Byte> and Image<Bgr, Byte> classes.
using Emgu.CV; // Import Emgu.CV namespace for image processing utilities.

// Class implementing the ISkinDetector interface for detecting skin regions in an image.
public class YCrCbSkinDetector : ISkinDetector
{
    // Method to detect skin regions in the provided BGR image and return a binary mask.
    public Image<Gray, Byte> DetectSkin(Image<Bgr, Byte> image)
    {
        // Convert the BGR image to YCrCb color space.
        Image<Ycc, Byte> currentYCrCbFrame = image.Convert<Ycc, Byte>();

        // Initialize an image to store the binary mask for skin regions.
        Image<Gray, Byte> skin = new Image<Gray, Byte>(image.Width, image.Height);

        // Access image data for efficient processing.
        byte[,,] YCrCbData = currentYCrCbFrame.Data;
        byte[,,] skinData = skin.Data;

        // Iterate through each pixel of the image.
        for (int i = 0; i < image.Rows; i++)
        {
            for (int j = 0; j < image.Cols; j++)
            {
                // Extract Y, Cr, and Cb components from the YCrCb color space.
                int y = YCrCbData[i, j, 0]; //luminance
                int cr = YCrCbData[i, j, 1]; //Red Difference
                int cb = YCrCbData[i, j, 2]; //Blue Difference

                // cb and cr are values used to detect skin and non-skin values
                // increase => less skin detected
                // decrease => more skin detected
                cb -= 109;
                cr -= 152;

                // Calculate a thresholding value based on Cr and Cb values.
                int x1 = (819 * cr - 614 * cb) / 32 + 51;
                int y1 = (819 * cr + 614 * cb) / 32 + 77;
                x1 = x1 * 41 / 1024;
                y1 = y1 * 73 / 1024;
                int value = x1 * x1 + y1 * y1;

                // Determine if the pixel is likely to be part of skin based on Y value and threshold.
                if (y < 100)
                    skinData[i, j, 0] = (byte)(value < 700 ? 255 : 0); // Skin if Y < 100 and threshold condition.
                else
                    skinData[i, j, 0] = (byte)(value < 850 ? 255 : 0); // Skin if Y >= 100 and threshold condition.
            }
        }

        // Apply morphological operations (erode and dilate) to refine the binary mask.
        var structuringElement = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new System.Drawing.Size(6, 6), new System.Drawing.Point(3, 3));
        CvInvoke.Erode(skin, skin, structuringElement, new System.Drawing.Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Reflect, default);
        CvInvoke.Dilate(skin, skin, structuringElement, new System.Drawing.Point(-1, -1), 2, Emgu.CV.CvEnum.BorderType.Reflect, default);

        return skin; // Return the binary mask representing skin regions.
    }
}
